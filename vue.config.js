module.exports = {
  devServer: {
    proxy: {
      '^/api/users': {
        target: 'http://localhost:8015',
        changeOrigin: true,
        pathRewrite: {'^/api': ''}
      },
      '^/api/auth': {
        target: 'http://localhost:8015',
        changeOrigin: true,
        pathRewrite: {'^/api': ''}
      },
      '^/api': {
        target: 'http://localhost:8080',
        changeOrigin: true,
        pathRewrite: {'^/api' : ''}
      }
    }
  },
  css: {
    extract: true,
    loaderOptions: {
      sass: {
        prependData: `@import "~@tromm/styleguide/src/components/base/core/bootstrap-required";`
      }
    }
  }
};
