import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home";
import Default from "@/views/layout/Default";
import AdminDB from "@/views/admin/AdminDB";
import AddDB from "@/views/admin/AddDB";
import ModifyDB from "@/views/admin/ModifyDB"
import UserDB from "@/views/admin/UserDB";
import Settings from "@/views/account/Settings";
import Account from "@/views/account/Account";
import Login from "@/views/auth/Login";
import Register from "@/views/auth/Register";
import Browse from "@/views/Browse";
import Recipe from "@/views/Recipe";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    component: Default,
    children: [
      {
        path: "/",
        name: "home",
        component: Home
      },
      {
        path: "/admin",
        name: "admin",
        component: AdminDB,
      },
      {
        path: "/admin/add",
        name: "admin-add",
        component: AddDB
      },
      {
        path: "/admin/modify",
        name: "admin-modify",
        component: ModifyDB
      },
      {
        path: "/admin/users",
        name: "admin-users",
        component: UserDB
      },
      {
        path: "/account",
        name: "account",
        component: Account
      },
      {
        path: "/account/instellingen",
        name: "settings",
        component: Settings
      },
      {
        path: "/login",
        name: "login",
        component: Login
      },
      {
        path: "/registreren",
        name: "register",
        component: Register
      },
      {
        path: "/recepten",
        name: "browse",
        component: Browse
      },
      {
        path: "/recept",
        name: "recipe",
        component: Recipe
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
