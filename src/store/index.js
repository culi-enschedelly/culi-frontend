import Vue from 'vue'
import Vuex from 'vuex'
import http from "@/services/http";

Vue.use(Vuex)

export default new Vuex.Store({
  getters: {
    isLoggedIn: state => state.isLoggedIn,
    user: state => state.user
  },
  state: {
    isLoggedIn: false,
    user: null
  },
  mutations: {
    setIsLoggedIn(state, isLoggedIn) {
      state.isLoggedIn = isLoggedIn;
    },
    setUser(state, user) {
      state.user = user;
    }
  },
  actions: {
    fetchUser({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/users/me').then(res => {
          commit('setUser', res.data.data);
          resolve();
        }).catch(e => reject(e));
      })
    }
  }
})
