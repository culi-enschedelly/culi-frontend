import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Components from "@tromm/styleguide";
import http from '@/services/http';
import VModal from 'vue-js-modal'

Vue.use(Components);
Vue.use(VModal);

Vue.prototype.$http = http.axios;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
